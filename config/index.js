
const path = require('path');

module.exports = {

    port: process.env.PORT || 8000,
    hostname: 'localhost',
    get fullHostname() {
        return `${this.hostname}:${this.port}`;
    },

    sequelize: {
        opt: {
            host: 'localhost',
            dialect: 'postgres',
            port: 5432,
            pool: {
                max: 10,
                min: 1,
                idle: 1000,
            },
            logging: function () {},
        },
        username: 'sidcmi',
        password: '',
        database: 'sidcmi',
        get connectionUri() {
            return `postgres://${this.username}:${this.password}@${this.opt.host}:${this.opt.port}/${this.database}`;
        },
    },

    // dir
    appDir: path.join(__dirname, '..'),
    uploadDir: path.join(__dirname, '..', '/assets/upload'),
    logDir: path.join(__dirname, '../logs/dev/'),

    // locale
    i18n: {
        defaultLocale: 'en_US',
    },

    // swig
    swig: {
        cache: false,
    },

    // nodemailer
    emailer: {
        service: 'emailService',
        user: 'username',
        pass: 'password',
    },

    cookie: {
        secret: 'thisisnotsecret',
    },
};
