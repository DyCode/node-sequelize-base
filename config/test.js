
const path = require('path');
const _ = require('lodash');

module.exports = _.merge(require('./'), {

    // database
    postgresql: {
        database: 'sidcmitest',
        username: 'sidcmitest',
    },

    logDir: path.join(__dirname, '../logs/test/'),
});
