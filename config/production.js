
const _ = require('lodash');

module.exports = _.merge(require('./'), {

    // database
    sequelize: {
        opt: {
            host: 'localhost',
            dialect: 'postgres',
            port: 5432,
        },
        username: 'sidcmi',
        password: '',
        database: 'sidcmi',
    },

    // most likely should change this
    logDir: '/var/log/app',

    // swig
    swig: {
        cache: 'memory',
    },

    // nodemailer
    emailer: {
        service: 'emailService',
        user: 'username',
        pass: 'password',
    },

    cookie: {
        secret: 'somesecretthatyoushouldkeep',
    },
});
