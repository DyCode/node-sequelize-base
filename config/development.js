
const _ = require('lodash');

module.exports = _.merge(require('./'), {
    sequelize: {
        opt: {
            benchmark: true,
            logging: console.log.bind(console),
        },
    },
});
