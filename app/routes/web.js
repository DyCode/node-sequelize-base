
const express = require('express');
const router = express.Router();

const IndexController = require('../controllers/IndexController');

// initialize passport strategies
require('../helpers/passport');

// Index
router.get('/', IndexController.index);

module.exports = router;
