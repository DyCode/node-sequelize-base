
const Sequelize = require('sequelize');
const sequelize = require('../helpers/database').getInstance();
const User = sequelize.define('user', {
    username: {
        type: Sequelize.STRING,
        primaryKey: true,
    },

    password: {
        type: Sequelize.STRING,
    },

    email: {
        type: Sequelize.STRING,
    },

    name: {
        type: Sequelize.STRING,
    },
});

module.exports = User;
