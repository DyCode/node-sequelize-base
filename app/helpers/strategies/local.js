
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../../models/user');
const crypto = require('crypto');

passport.use(new LocalStrategy((username, password, done) => {
    User.findById(username).asCallback((err, user) => {
        if (err) return done(err);

        if (!user)
            return done(null, false, { message: 'User is not found' });

        const hashed = crypto
            .createHash('sha256')
            .update(password)
            .digest('hex');

        if (user.password !== hashed)
            return done(null, false, { message: 'Invalid password' });

        return done(null, user);
    });
}));
