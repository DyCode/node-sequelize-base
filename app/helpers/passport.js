'use strict';

const passport = require('passport');

require('./strategies/local');

passport.serializeUser((user, done) => {
    done(null, user);
});

passport.deserializeUser((user, done) => {
    done(null, user);
});
