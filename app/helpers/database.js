'use strict';

const Sequelize = require('sequelize');
const config = require('../../config/' + (process.env.NODE_ENV || 'development'));

let sequelizeInstance = null;

module.exports = {
    getInstance() {
        if (sequelizeInstance) {
            return sequelizeInstance;
        }

        sequelizeInstance = new Sequelize(
            config.sequelize.database,
            config.sequelize.username,
            config.sequelize.password,
            config.sequelize.opt
        );

        return sequelizeInstance;
    },
};
