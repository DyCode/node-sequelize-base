'use strict';

const async = require('async');
const _ = require('lodash');
const fs = require('fs');
const path = require('path');

const validJsFileRegex = /\.js$/;
const modelDirectory = path.join(__dirname, '../models');
const noOp = () => {};

module.exports = {
    loadAndSync(modelName, syncOptions, callback) {
        const doneCallback = typeof callback === 'function' ? callback : noOp;

        if (_.isArray(modelName)) {
            const returnValues = {};

            return async.each(modelName, (model, done) => {
                const loadedModel = require(modelDirectory + '/' + model);
                returnValues[model.replace(validJsFileRegex, '')] = loadedModel;

                loadedModel.sync(syncOptions).asCallback(err => {
                    if (err) return done(err);
                    return done(null);
                });
            },

            err => {
                if (err) return doneCallback(err);
                return doneCallback(null, returnValues);
            });
        }

        const loadedModel = require(modelDirectory + '/' + modelName);

        return loadedModel.sync(syncOptions).asCallback(err => {
            if (err) return doneCallback(err);
            return doneCallback(null, loadedModel);
        });
    },

    loadAllAndSync(syncOptions, callback) {
        const doneCallback = typeof callback === 'function' ? callback : noOp;

        fs.readdir(modelDirectory, (err, models) => {
            const modelFiles = _.filter(models, model => validJsFileRegex.test(model));

            return this.loadAndSync(modelFiles, syncOptions, doneCallback);
        });
    },
};
